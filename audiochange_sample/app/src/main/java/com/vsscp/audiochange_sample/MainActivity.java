package com.vsscp.audiochange_sample;

import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mAudioChangeButton;
    public static boolean mSlient = false;
    public static int mPreRingMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        mAudioChangeButton = (Button) findViewById(R.id.btn_audiochange);
        mAudioChangeButton.setOnClickListener(this);

        MyPhoneStateListener phoneListener = new MyPhoneStateListener(this);
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_audiochange:
                Intent intent = new Intent(this, NotificationService.class);
                if (mSlient == true) {
                    intent.putExtra("silent",false);
                    mAudioChangeButton.setText("Go Silent Mode");
                    mSlient = false;
                } else {
                    intent.putExtra("silent",true);
                    mAudioChangeButton.setText("Go Unsilent Mode");
                    mSlient = true;
                    AudioManager mAudioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
                    mPreRingMode = mAudioManager.getRingerMode();
                }
                startService(intent);
                break;
        }
    }

//    private void SilentAudio(){
//        mSlient = true;
//        AudioManager mAudioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
//        mPreRingMode = mAudioManager.getRingerMode();
////        mAudioManager.setRingerMode(mAudioManager.RINGER_MODE_SILENT);
////        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
////            mAudioManager.adjustStreamVolume(AudioManager.STREAM_NOTIFICATION, AudioManager.ADJUST_MUTE, 0);
////            mAudioManager.adjustStreamVolume(AudioManager.STREAM_ALARM, AudioManager.ADJUST_MUTE, 0);
////            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, 0);
//////            mAudioManager.adjustStreamVolume(AudioManager.STREAM_RING, AudioManager.ADJUST_MUTE, 0);
////            mAudioManager.adjustStreamVolume(AudioManager.STREAM_SYSTEM, AudioManager.ADJUST_MUTE, 0);
////        } else {
////            mAudioManager.setStreamMute(AudioManager.STREAM_NOTIFICATION, true);
////            mAudioManager.setStreamMute(AudioManager.STREAM_ALARM, true);
////            mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
//////            mAudioManager.setStreamMute(AudioManager.STREAM_RING, true);
////            mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
////        }
//    }
//
//    private void UnSilentAudio(){
//        mSlient = false;
////        AudioManager mAudioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
////        if (mPreRingMode == mAudioManager.RINGER_MODE_NORMAL) {
////            mAudioManager.setRingerMode(mAudioManager.RINGER_MODE_NORMAL);
////        } else if (mPreRingMode == mAudioManager.RINGER_MODE_VIBRATE) {
////            mAudioManager.setRingerMode(mAudioManager.RINGER_MODE_VIBRATE);
////        }
////        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
////            mAudioManager.adjustStreamVolume(AudioManager.STREAM_NOTIFICATION, AudioManager.ADJUST_UNMUTE, 0);
////            mAudioManager.adjustStreamVolume(AudioManager.STREAM_ALARM, AudioManager.ADJUST_UNMUTE, 0);
////            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_UNMUTE,0);
//////            mAudioManager.adjustStreamVolume(AudioManager.STREAM_RING, AudioManager.ADJUST_UNMUTE, 0);
////            mAudioManager.adjustStreamVolume(AudioManager.STREAM_SYSTEM, AudioManager.ADJUST_UNMUTE, 0);
////        } else {
////            mAudioManager.setStreamMute(AudioManager.STREAM_NOTIFICATION, false);
////            mAudioManager.setStreamMute(AudioManager.STREAM_ALARM, false);
////            mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
//////            mAudioManager.setStreamMute(AudioManager.STREAM_RING, false);
////            mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
////        }
//    }
}
