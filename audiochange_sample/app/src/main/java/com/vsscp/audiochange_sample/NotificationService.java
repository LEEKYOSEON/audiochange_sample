package com.vsscp.audiochange_sample;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

/**
 * Created by VSSCP_KYOSEON on 2016. 1. 11..
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class NotificationService extends NotificationListenerService {

	private Integer key = 1;

	public NotificationService() {
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	@Override
	public void onNotificationPosted(StatusBarNotification sbn) {
		Log.i("lks", "SNSNotificationService:onNotificationPosted");
		Log.i("lks", "sbn.getPackageName() = "+sbn.getPackageName());

	}

	@Override
	public void onNotificationRemoved(StatusBarNotification sbn) {
		super.onNotificationRemoved(sbn);
		Log.i("lks", "SNSNotificationService:onNotificationRemoved");
	}

	@Override
	public StatusBarNotification[] getActiveNotifications() {
		Log.i("lks", "SNSNotificationService:getActiveNotifications");
		return super.getActiveNotifications();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		boolean start = intent.getBooleanExtra("silent", false);
		if(start)
		{
			Log.d("lks","START");

			//Check if at least Lollipop, otherwise use old method
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
				requestInterruptionFilter(INTERRUPTION_FILTER_NONE);
			else{
				AudioManager am = (AudioManager) getBaseContext().getSystemService(AUDIO_SERVICE);
				am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
			}
		}
		else
		{
			Log.d("TAG","STOP");
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
				requestInterruptionFilter(INTERRUPTION_FILTER_ALL);
			else{
				AudioManager mAudioManager = (AudioManager) getBaseContext().getSystemService(AUDIO_SERVICE);
				if (MainActivity.mPreRingMode == mAudioManager.RINGER_MODE_NORMAL) {
					mAudioManager.setRingerMode(mAudioManager.RINGER_MODE_NORMAL);
				} else if (MainActivity.mPreRingMode == mAudioManager.RINGER_MODE_VIBRATE) {
					mAudioManager.setRingerMode(mAudioManager.RINGER_MODE_VIBRATE);
				}
			}
		}
		return super.onStartCommand(intent, flags, startId);

	}
}
