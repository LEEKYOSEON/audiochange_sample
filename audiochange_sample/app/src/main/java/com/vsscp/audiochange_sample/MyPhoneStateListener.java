package com.vsscp.audiochange_sample;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.VideoView;

/**
 * Created by VSSCP_KYOSEON on 2016. 1. 11..
 */
public class MyPhoneStateListener extends PhoneStateListener {
	private Context mContext;
	private boolean mChangeFromSlient = false;
	private long[] pattern = {100,400,100,400,100,400}; // miliSecond	// 짝수 인덱스 : 대기시간 // 홀수 인덱스 : 진동시간
	private static MediaPlayer mp;
	private static int mCurrentVolume;

	public MyPhoneStateListener() {
		super();
	}

	public MyPhoneStateListener(Context context) {
		mContext = context;
	}

	@Override
	public void onCallStateChanged(int state, String incomingNumber) {
		super.onCallStateChanged(state, incomingNumber);

		Log.i("LKS", "MyPhoneStateListener : state = " + state + " incomingNumber = " + incomingNumber);

		AudioManager mAudioManager = (AudioManager) mContext.getSystemService(mContext.AUDIO_SERVICE);
//		Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
//		Ringtone mRingtone = RingtoneManager.getRingtone(mContext, notification);
		Vibrator mVibrator = (Vibrator)mContext.getSystemService(Context.VIBRATOR_SERVICE);
		Uri defaultRintoneUri = RingtoneManager.getActualDefaultRingtoneUri(mContext, RingtoneManager.TYPE_RINGTONE);
		if (mp == null) {
			mp = MediaPlayer.create(mContext, defaultRintoneUri);
		}

		if (state == TelephonyManager.CALL_STATE_RINGING) {	//전화 들어올 때
			if (MainActivity.mSlient) {	//내가 설정한 상태가 무음일때
				if (MainActivity.mPreRingMode == mAudioManager.RINGER_MODE_NORMAL) {    //이전 모드가 벨소리 이면
//					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//						mAudioManager.adjustStreamVolume(AudioManager.STREAM_RING, AudioManager.ADJUST_UNMUTE, 0);
//					} else {
//						mAudioManager.setStreamMute(AudioManager.STREAM_RING, false);
//					}
//					int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
//					mAudioManager.setStreamVolume(AudioManager.STREAM_RING, maxVolume, 0);
//					mRingtone.play();

					mCurrentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
					mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
					mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
					mp.setLooping(true);
					mp.start();
				} else if (MainActivity.mPreRingMode == mAudioManager.RINGER_MODE_VIBRATE) {    //이전 모드가 진동이면
					if (mVibrator != null) {
						mVibrator.vibrate(pattern, 0);    // 0 : 무한반복, -1: 반복없음,
					}
				}
			}

		} else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {	//전화 받았을 때
			if (MainActivity.mSlient) {	//내가 설정한 상태가 무음일때
				if (MainActivity.mPreRingMode == mAudioManager.RINGER_MODE_NORMAL) {    //이전 모드가 벨소리 이면
//					if (mRingtone != null && mRingtone.isPlaying()) {
//						mRingtone.stop();
//					}

					if (mp != null && mp.isPlaying()) {
						mp.stop();
						mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mCurrentVolume, 0);
						mp = null;
					}
				} else if (MainActivity.mPreRingMode == mAudioManager.RINGER_MODE_VIBRATE) {    //이전 모드가 진동이면
					if (mVibrator != null) {
						mVibrator.cancel();
					}
				}
				mAudioManager.setRingerMode(MainActivity.mPreRingMode);    //이전 모드로 회복
			}

		} else {	//전화 끊었거나 평상시에
			if (MainActivity.mSlient) {	//내가 설정한 상태가 무음일때
				if (MainActivity.mPreRingMode == mAudioManager.RINGER_MODE_NORMAL) {	//이전 모드가 벨소리 이고
//					if (mRingtone != null && mRingtone.isPlaying()) {        //벨이 울리는 중이면
//						mRingtone.stop();
//						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//							mAudioManager.adjustStreamVolume(AudioManager.STREAM_RING, AudioManager.ADJUST_MUTE, 0);
//						} else {
//							mAudioManager.setStreamMute(AudioManager.STREAM_RING, true);
//						}
//					}

					if (mp != null && mp.isPlaying()) {
						mp.stop();
						mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mCurrentVolume, 0);
						mp = null;
					}
				} else if (MainActivity.mPreRingMode == mAudioManager.RINGER_MODE_VIBRATE) {	//이전 모드가 진동이면
					if (mVibrator != null) {
						mVibrator.cancel();
					}
				}
			}
		}
//		if (state != 0) {
//			if (mAudioManager.getRingerMode() == mAudioManager.RINGER_MODE_SILENT) {
//				mAudioManager.setRingerMode(mAudioManager.RINGER_MODE_NORMAL);
//				mChangeFromSlient = true;
//			}
//		} else {
//			if (mChangeFromSlient) {
//				mAudioManager.setRingerMode(mAudioManager.RINGER_MODE_SILENT);
//				mChangeFromSlient = false;
//			}
//		}

	}
}
